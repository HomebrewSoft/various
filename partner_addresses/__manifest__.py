{
    "name": "Partner Addresses",
    "version": "13.0.0.1.0",
    "author": "HomebrewSoft",
    "website": "https://HomebrewSoft/dev/",
    "license": "LGPL-3",
    "depends": [
        "account",
        "purchase",
        "sale",
    ],
    "data": [
        # security
        # data
        # reports
        # views
        "views/sale_order.xml",
        "views/purchase_order.xml",
        "views/account_move.xml",
    ],
}
